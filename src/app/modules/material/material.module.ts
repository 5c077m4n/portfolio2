import { NgModule } from '@angular/core';
import { ScrollingModule, ScrollDispatchModule } from '@angular/cdk/scrolling';
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatSliderModule } from '@angular/material/slider';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatToolbarModule } from '@angular/material/toolbar';


const modules = [
	MatCardModule,
	MatDividerModule,
	MatButtonModule,
	MatIconModule,
	MatTooltipModule,
	MatSliderModule,
	MatFormFieldModule,
	MatInputModule,
	ScrollingModule,
	ScrollDispatchModule,
	MatToolbarModule
];

@NgModule({
	imports: [
		...modules
	],
	exports: [
		...modules
	],
})
export class MaterialModule { }
