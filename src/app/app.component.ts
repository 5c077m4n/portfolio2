import { Component, OnInit } from '@angular/core';
import ScrollOut from 'scroll-out';

import { IconRegistrationService } from '@services/icon-registration/icon-registration.service';


@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.less'],
	providers: [IconRegistrationService],
})
export class AppComponent implements OnInit {
	constructor(private iconReg: IconRegistrationService) { }
	ngOnInit(): void {
		ScrollOut({ once: true });
	}
}
