import {
	Component, ChangeDetectorRef, OnInit, OnDestroy, ChangeDetectionStrategy
} from '@angular/core';
import { Observable, fromEvent, Subscription } from 'rxjs';
import { debounceTime, startWith, tap, map } from 'rxjs/operators';

import { ReferencesService } from '@services/references/references.service';
import { ProjectsService } from '@services/projects/projects.service';
import { NavigationService } from '@services/navigation/navigation.service';


@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.less'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class HomeComponent implements OnInit, OnDestroy {
	public projects: any[][] = [[]];
	public x = 0;
	public y = 0;
	private listener: Subscription;

	constructor(
		private cdr: ChangeDetectorRef,
		private navService: NavigationService,
		private refService: ReferencesService,
		private projService: ProjectsService,
	) {
		this.listener = new Subscription();
	}
	ngOnInit(): void {
		this.listener.add(
			this.projService.get10NewestProjects()
				.pipe(
					map(resp => resp && resp.values || []),
					tap(respArr => this.projects[0] = respArr),
					tap(_ => this.cdr.detectChanges())
				)
				.subscribe()
		);
		this.listener.add(
			this.projService.getNpmioPackages()
				.pipe(
					map(resp => resp && resp.results || []),
					tap(respArr => this.projects[1] = respArr),
					tap(_ => this.cdr.detectChanges()),
				)
				.subscribe()
		);
		this.listener.add(
			this.navService.keyPres$
				.pipe(tap(this.onKeyPress))
				.subscribe()
		);
		this.listener.add(
			this.navService.mouseScroll$
				.pipe(tap(this.onMouseScroll))
				.subscribe()
		);
	}

	private moveRight(): void {
		(this.x >= this.projects[this.y].length - 1) ? this.x++ : this.x = 0;
	}
	private moveLeft(): void {
		(this.x <= 0) ? this.x = this.projects[this.y].length - 1 : this.x--;
	}
	private moveUp(): void {
		this.y >= this.projects.length ? 0 : 1;
	}
	private moveDown(): void {
		this.y <= 0 ? 1 : 0;
	}

	private get screenWidth$(): Observable<number> {
		return fromEvent(this.refService.window, 'resize')
			.pipe(
				startWith(this.refService.window.innerWidth),
				debounceTime(500),
				map((e: any) => e.target.innerWidth)
			);
	}

	private onKeyPress(key: string): void {
		switch (key) {
			case 'ArrowLeft':
				this.moveLeft();
				break;
			case 'ArrowRight':
				this.moveRight();
				break;
			case 'ArrowUp':
				this.moveUp();
				break;
			case 'ArrowDown':
				this.moveDown();
				break;
			default:
				return;
		}
		this.cdr.detectChanges();
	}
	private onMouseScroll({ deltaX, deltaY }): void {
		if (deltaX > 0 && deltaY === 0) this.moveLeft();
		else if (deltaX < 0 && deltaY === 0) this.moveRight();
		else if (deltaX === 0 && deltaY > 0) this.moveUp();
		else if (deltaX === 0 && deltaY < 0) this.moveDown();
		else return;

		this.cdr.detectChanges();
	}

	ngOnDestroy(): void {
		this.listener.unsubscribe();
	}
}
