import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';


@Component({
	selector: 'app-main-navbar',
	templateUrl: './main-navbar.component.html',
	styleUrls: ['./main-navbar.component.less'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class MainNavbarComponent implements OnInit {
	constructor() { }
	ngOnInit() { }
}
