import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';


@Component({
	selector: 'app-project-item',
	templateUrl: './project-item.component.html',
	styleUrls: ['./project-item.component.less'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProjectItemComponent implements OnInit {
	@Input() public project: any;
	constructor() { }
	ngOnInit() { }
}
