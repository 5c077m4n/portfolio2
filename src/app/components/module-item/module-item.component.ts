import {
	Component, OnInit, ChangeDetectionStrategy, Input, ViewChild, ElementRef, AfterViewInit
} from '@angular/core';
import Chart from 'chart.js';


@Component({
	selector: 'app-module-item',
	templateUrl: './module-item.component.html',
	styleUrls: ['./module-item.component.less'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class ModuleItemComponent implements OnInit, AfterViewInit {
	@Input() public module: any;
	public moduleChart: Chart;
	@ViewChild('moduleChart') private ctx: ElementRef;
	constructor() { }
	ngOnInit() { }
	ngAfterViewInit() {
		Chart.defaults.global.defaultFontColor = 'black';
		this.moduleChart = new Chart(this.ctx.nativeElement, {
			type: 'line',
			data: {
				labels: this.module.packageDetails.collected
					.npm
					.downloads
					.map(data => data.midDate.toLocaleDateString('en-GB')),
				datasets: [{
					label: '# of Downloads',
					data: this.module.packageDetails.collected
						.npm
						.downloads.map(data => data.count),
					backgroundColor: [],
					borderColor: [],
					borderWidth: 1
				}]
			},
			options: {
				elements: { line: { tension: 0 } },
				scales: {
					yAxes: [{ ticks: { beginAtZero: true } }]
				},
				showLines: false,
			}
		});
	}

	ngOnDestroy(): void {
		this.moduleChart.destroy();
	}
}
