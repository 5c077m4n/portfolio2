import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ServiceWorkerModule } from '@angular/service-worker';

import { environment } from '@environment';
import { AppRoutingModule } from '@routing';
import { MaterialModule } from '@modules/material/material.module';
import { AppComponent } from './app.component';
import { ProjectItemComponent } from '@components/project-item/project-item.component';
import { ModuleItemComponent } from '@components/module-item/module-item.component';
import { MainNavbarComponent } from '@components/main-navbar/main-navbar.component';
import { HomeComponent } from '@components/home/home.component';
import { FormatNumbersPipe } from '@pipes/format-numbers/format-numbers.pipe';


@NgModule({
	declarations: [
		AppComponent,
		FormatNumbersPipe,
		ProjectItemComponent,
		ModuleItemComponent,
		MainNavbarComponent,
		HomeComponent
	],
	imports: [
		HttpClientModule,
		BrowserModule,
		AppRoutingModule,
		BrowserAnimationsModule,
		ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
		MaterialModule,
		FlexLayoutModule
	],
	providers: [],
	bootstrap: [AppComponent]
})
export class AppModule { }
