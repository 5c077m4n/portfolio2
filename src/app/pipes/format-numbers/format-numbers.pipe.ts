import { Pipe, PipeTransform } from '@angular/core';


@Pipe({ name: 'formatNumbers' })
export class FormatNumbersPipe implements PipeTransform {
	transform(value: number, args?: any): string {
		return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}
}
