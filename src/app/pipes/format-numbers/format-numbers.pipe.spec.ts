import { FormatNumbersPipe } from './format-numbers.pipe';


let pipe: FormatNumbersPipe;

fdescribe('FormatNumbersPipe', () => {
	beforeAll(() => pipe = new FormatNumbersPipe());

	it('create an instance', () => {
		expect(pipe).toBeTruthy();
	});
	it('should leave 99 as 99', () => {
		expect(pipe.transform(99)).toEqual('99');
	});
	it('should convert 999999 to 999,999', () => {
		expect(pipe.transform(999999)).toEqual('999,999');
	});
	it('should convert -999999 to -999,999', () => {
		expect(pipe.transform(-999999)).toEqual('-999,999');
	});
});
