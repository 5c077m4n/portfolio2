import { TestBed } from '@angular/core/testing';

import { IconRegistrationService } from './icon-registration.service';


describe('RegisterIconsService', () => {
	beforeEach(() => TestBed.configureTestingModule({}));

	it('should be created', () => {
		const service: IconRegistrationService = TestBed.get(IconRegistrationService);
		expect(service).toBeTruthy();
	});
});
