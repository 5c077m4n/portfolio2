import { Injectable } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';


@Injectable() export class IconRegistrationService {
	constructor(
		private iconRegistry: MatIconRegistry,
		private sanitizer: DomSanitizer
	) {
		this.iconRegistry.addSvgIcon(
			'linkedIn',
			this.sanitizer.bypassSecurityTrustResourceUrl('assets/icons/linkedIn.svg')
		);
		this.iconRegistry.addSvgIcon(
			'resume',
			this.sanitizer.bypassSecurityTrustResourceUrl('assets/icons/resume.svg')
		);
		this.iconRegistry.addSvgIcon(
			'code',
			this.sanitizer.bypassSecurityTrustResourceUrl('assets/icons/code.svg')
		);
		this.iconRegistry.addSvgIcon(
			'code',
			this.sanitizer.bypassSecurityTrustResourceUrl('assets/icons/code.svg')
		);
		this.iconRegistry.addSvgIcon(
			'new_tab',
			this.sanitizer.bypassSecurityTrustResourceUrl('assets/icons/new_tab.svg')
		);
		this.iconRegistry.addSvgIcon(
			'github',
			this.sanitizer.bypassSecurityTrustResourceUrl('assets/icons/github.svg')
		);
		this.iconRegistry.addSvgIcon(
			'bitbucket',
			this.sanitizer.bypassSecurityTrustResourceUrl('assets/icons/bitbucket.svg')
		);
		this.iconRegistry.addSvgIcon(
			'npm',
			this.sanitizer.bypassSecurityTrustResourceUrl('assets/icons/npm.svg')
		);
		this.iconRegistry.addSvgIcon(
			'stats',
			this.sanitizer.bypassSecurityTrustResourceUrl('assets/icons/stats.svg')
		);
		this.iconRegistry.addSvgIcon(
			'email',
			this.sanitizer.bypassSecurityTrustResourceUrl('assets/icons/email.svg')
		);
		this.iconRegistry.addSvgIcon(
			'arrow-up',
			this.sanitizer.bypassSecurityTrustResourceUrl('assets/icons/arrow-up.svg')
		);
		this.iconRegistry.addSvgIcon(
			'arrow-down',
			this.sanitizer.bypassSecurityTrustResourceUrl('assets/icons/arrow-down.svg')
		);
	}
}
