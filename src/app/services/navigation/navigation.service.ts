import { Injectable } from '@angular/core';
import { Observable, fromEvent } from 'rxjs';
import { map, filter } from 'rxjs/operators';

import { ReferencesService } from '@services/references/references.service';


@Injectable({ providedIn: 'root' })
export class NavigationService {
	constructor(private refService: ReferencesService) { }

	public get keyPres$(): Observable<string> {
		return fromEvent(this.refService.document, 'keydown')
			.pipe(
				map((e: KeyboardEvent) => e.key),
				filter((key: string) => (key === 'ArrowLeft' || key === 'ArrowRight' || key === 'ArrowUp' || key === 'ArrowDown')),
			);
	}
	public get mouseScroll$(): Observable<{ deltaX: number, deltaY: number }> {
		return fromEvent(this.refService.document, 'wheel')
			.pipe(
				map((e: MouseWheelEvent) => ({ deltaX: e.deltaX, deltaY: e.deltaY })),
			);
	}
}
