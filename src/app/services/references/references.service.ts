import { Injectable } from '@angular/core';


@Injectable({ providedIn: 'root' })
export class ReferencesService {
	private readonly windowRef = window;
	private readonly docRef = document;
	constructor() { }

	public get window(): Window {
		return this.windowRef;
	}
	public get document(): Document {
		return this.docRef;
	}
}
